package com.example.bai72.service;

import com.example.bai72.model.StudentAverageScore;
import com.example.bai72.model.StudentEntity;
import com.example.bai72.repository.StudentRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

@Component
public class StudentImpl implements StudentInter{

    @Autowired
    private StudentRepo studentRepo;

    @Override
    public List<StudentEntity> getListAlphabetSorted() {
        List<StudentEntity> listStudent = studentRepo.findAll();
        return listStudent.stream()
                .sorted(Comparator.comparing(StudentEntity::getName))
                .collect(Collectors.toList());
    }

    @Override
    public List<StudentAverageScore> getListWithAverageScore() {
        return studentRepo.findAll().stream()
                .map(student -> new StudentAverageScore(student.getName(), student.getAverage()))
                .collect(Collectors.toList());
    }

    @Override
    public HashMap<String, String> getStudentWithHighestScore() {
        List<StudentEntity> listStudent = studentRepo.findAll();
        HashMap<String, String> leaderboard = new HashMap<>();
        float highestMathScore = 0;
        float highestChemistryScore = 0;
        float highestPhysicsScore = 0;

        for(StudentEntity student : listStudent){
            if(student.getMath() > highestMathScore) {
                highestMathScore = student.getMath();
                leaderboard.put("Hoc sinh co diem Toan cao nhat:", student.getName());
            }
            if(student.getPhysics() > highestPhysicsScore) {
                highestPhysicsScore = student.getPhysics();
                leaderboard.put("Hoc sinh co diem Vat ly cao nhat:", student.getName());
            }
            if(student.getChemistry() > highestChemistryScore) {
                highestChemistryScore = student.getChemistry();
                leaderboard.put("Hoc sinh co diem Hoa cao nhat:", student.getName());
            }
        }
        return leaderboard;
    }
    }

    @Override
    public Map<String, Float> calculateSumOfStudentsWithAverageMoreThan5() {
        List<StudentEntity> listStudent = studentRepo.findAllByOrderByIdDesc();
        return listStudent.stream()
                .filter(student -> student.getAverage() >= 5)
                .collect(Collectors.toMap(StudentEntity::getName,
                        student -> student.getPhysics() + student.getChemistry() + student.getMath()));
    }

    @Override
    public List<StudentEntity> reverseList() {
        return studentRepo.findAllByOrderByIdDesc();
    }
}