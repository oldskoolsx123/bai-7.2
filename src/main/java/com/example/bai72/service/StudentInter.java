package com.example.bai72.service;

import com.example.bai72.model.StudentAverageScore;
import com.example.bai72.model.StudentEntity;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

@Service
public interface StudentInter {

    List<StudentEntity> getListAlphabeltSorted();
    List<StudentAverageScore> getListWithAverageScore();
    HashMap<String, String> getStudentWithHighestScore();
    HashMap<String, Float> calculateSumOfStudentsWithAverageMoreThan5();
    List<StudentEntity> reverseList();

}
