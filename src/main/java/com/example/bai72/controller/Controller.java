package com.example.bai72.controller;

import com.example.bai72.service.StudentInter;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping("calling")
public class Controller {

    private final StudentInter studentInter;

    public Controller(StudentInter studentInter) {
        this.studentInter = studentInter;
    }

    @GetMapping("GetAlpha")
    ResponseEntity getAlphabetical(){
        return new ResponseEntity<>(studentInter.getListAlphabeltSorted(), HttpStatus.BAD_REQUEST);
    }

    @GetMapping("GetAverage")
    ResponseEntity getAverage(){
        return new ResponseEntity<>(studentInter.getListWithAverageScore(), HttpStatus.BAD_REQUEST);
    }

    @GetMapping("GetHighest")
    ResponseEntity getHighest(){
        return new ResponseEntity<>(studentInter.getStudentWithHighestScore(), HttpStatus.BAD_REQUEST);
    }

    @GetMapping("AverageHigherThan5")
    ResponseEntity averageHigherThan5(){
        return new ResponseEntity<>(studentInter.calculateSumOfStudentsWithAverageMoreThan5(), HttpStatus.BAD_REQUEST);
    }

    @GetMapping("ReverseList")
    ResponseEntity reverseList(){
        return new ResponseEntity<>(studentInter.reverseList(), HttpStatus.BAD_REQUEST);
    }
}
