package com.example.bai72.repository;

import com.example.bai72.model.StudentEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudentRepo extends JpaRepository<StudentEntity, Integer> {

    List<StudentEntity> findAllByOrderByIdDesc();
}
